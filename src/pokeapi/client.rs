use hyper::{Body, Client, Response, StatusCode, Uri};
use hyper::body::Buf;
use hyper::client::HttpConnector;
use hyper::http::uri::InvalidUri;
use hyper_tls::HttpsConnector;
use url::Url;

use crate::pokeapi::client::PokeError::{PokemonNotFound, APIError};
use crate::pokeapi::dto::PokemonSpecies;

/// Pokemon identifier
pub enum PokemonId {
    #[allow(dead_code)]
    Id(u32),
    Name(String),
}

#[derive(thiserror::Error, Debug)]
pub enum PokeError {
    #[error("Pokemon not found")]
    PokemonNotFound,
    #[error("API error")]
    APIError(StatusCode),
    #[error("Failed to parse Pokemon JSON")]
    JsonParse(#[from] serde_json::Error),
    #[error(transparent)]
    UrlParseError(#[from] url::ParseError),
    #[error(transparent)]
    UriParseError(#[from] InvalidUri),
    #[error(transparent)]
    CommunicationError(#[from] hyper::Error),
}

/// An HTTP client for the pokemon APIs
pub struct PokeClient {
    client: Client<HttpsConnector<HttpConnector>>,
    base_uri: String,
}

impl PokeClient {
    /// Instances a new `PokeClient`
    pub fn new() -> PokeClient {
        let https = HttpsConnector::new();
        let client = Client::builder()
            .build::<_, hyper::Body>(https);
        let base_uri = "https://pokeapi.co/api/v2/pokemon-species/".to_string();
        PokeClient { client, base_uri }
    }

    /// Catches a single Pokemon
    ///
    /// # Arguments
    /// * `id` - A Pokemon identifier (either ID or name)
    pub async fn catch(&self, id: &PokemonId) -> Result<PokemonSpecies, PokeError> {
        let id_param = match id {
            PokemonId::Id(id) => id.to_string(),
            PokemonId::Name(name) => name.to_lowercase()
        };
        let url = Url::parse(&self.base_uri)
            .and_then(|u| u.join(&id_param))?;
        let uri = url.as_str().parse::<Uri>()?;
        let res = (&self.client).get(uri).await?;

        match res.status() {
            s if s.is_success() => self.parse_response(res).await,
            StatusCode::NOT_FOUND => Err(PokemonNotFound),
            s => Err(APIError(s))
        }
    }

    async fn parse_response(&self, res: Response<Body>) -> Result<PokemonSpecies, PokeError> {
        let body = hyper::body::aggregate(res).await?;
        let pokemon: PokemonSpecies = serde_json::from_reader(body.reader())?;
        Ok(pokemon)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn catch_pikachu() {
        let client = PokeClient::new();
        let id = PokemonId::Name("pikachu".to_string());
        let pika = client.catch(&id).await.unwrap();
    }

    #[tokio::test]
    async fn catch_em_all() {
        let pokes = vec!(
            "Charmander",
            "squirtle",
            "raichu",
            "arbok",
            "spearow",
            "raticate",
            "weedle"
        );
        let client = PokeClient::new();
        for poke in pokes {
            print!("Gonna catch {0} ...", poke);
            let species = client.catch(&PokemonId::Name(poke.to_string())).await.unwrap();
            println!(" caught! (id={0}, is_legendary={1})", species.id, species.is_legendary);
        }
    }
}
