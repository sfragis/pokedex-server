use hyper::{Body, Client, header, Method, Request, Response, StatusCode, Uri};
use hyper::body::Buf;
use hyper::client::HttpConnector;
use hyper::http::uri::InvalidUri;
use hyper_tls::HttpsConnector;
use url::Url;
use url::form_urlencoded::Serializer;

use TranslationError::APIError;

use crate::funtranslations::dto::Translation;

/// The author affects the style of the translation
pub enum TranslationAuthor {
    Shakespeare,
    Yoda,
}

#[derive(thiserror::Error, Debug)]
pub enum TranslationError {
    #[error("API error")]
    APIError(StatusCode),
    #[error("Failed to parse translation JSON")]
    JsonParse(#[from] serde_json::Error),
    #[error(transparent)]
    UrlParseError(#[from] url::ParseError),
    #[error(transparent)]
    UriParseError(#[from] InvalidUri),
    #[error(transparent)]
    CommunicationError(#[from] hyper::Error),
    #[error(transparent)]
    HttpError(#[from] hyper::http::Error),
}

/// An HTTP client for the pokemon APIs
pub struct TranslationClient {
    client: Client<HttpsConnector<HttpConnector>>,
    base_uri: String,
}

impl TranslationClient {
    /// Instances a new `TranslationClient`
    pub fn new() -> TranslationClient {
        let https = HttpsConnector::new();
        let client = Client::builder()
            .build::<_, hyper::Body>(https);
        let base_uri = "https://api.funtranslations.com/translate/".to_string();
        TranslationClient { client, base_uri }
    }

    /// Translates some text
    ///
    /// # Arguments
    /// * `text` - Text to be translated
    pub async fn translate(&self, text: &str, author: TranslationAuthor) -> Result<Translation, TranslationError> {
        let endpoint = match author {
            TranslationAuthor::Shakespeare => "shakespeare.json",
            TranslationAuthor::Yoda => "yoda.json"
        };
        let url = Url::parse(&self.base_uri)
            .and_then(|u| u.join(endpoint))?;
        let uri = url.as_str().parse::<Uri>()?;

        let body = Serializer::new(String::new())
            .append_pair("text", text)
            .finish();

        let req = Request::builder()
            .method(Method::POST)
            .uri(uri)
            .header(header::CONTENT_TYPE, "application/x-www-form-urlencoded")
            .header(header::ACCEPT, "application/json")
            .body(Body::from(body))?;

        let res = (&self.client).request(req).await?;
        match res.status() {
            s if s.is_success() => self.parse_response(res).await,
            s => Err(APIError(s))
        }
    }

    async fn parse_response(&self, res: Response<Body>) -> Result<Translation, TranslationError> {
        let body = hyper::body::aggregate(res).await?;
        let trans: Translation = serde_json::from_reader(body.reader())?;
        Ok(trans)
    }
}

#[cfg(test)]
mod test {
    use crate::funtranslations::client::TranslationAuthor::{Shakespeare, Yoda};

    use super::*;

    #[tokio::test]
    async fn shakespeare_reads_dante() {
        let client = TranslationClient::new();
        let text = r#"When I had journeyed half of our life’s way,
                            I found myself within a shadowed forest,
                            for I had lost the path that does not stray."#;
        let trans = client.translate(text, Shakespeare).await.unwrap();
        println!("Shakespeare translation: {:?}", trans);
    }

    #[tokio::test]
    async fn yoda_reads_darth_vader() {
        let client = TranslationClient::new();
        let text = r#"The Force is with you, young Skywalker, but you are not a Jedi yet."#;
        let trans = client.translate(text, Yoda).await.unwrap();
        println!("Yoda translation: {:?}", trans);
    }
}
