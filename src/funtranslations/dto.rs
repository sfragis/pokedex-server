use serde::{Deserialize, Serialize};

// DTOs copied from https://pokeapi.co/docs/v2#pokemon-species

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Success {
    pub total: u16,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Contents {
    pub translated: String,
    pub text: String,
    pub translation: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Translation {
    pub success: Success,
    pub contents: Contents,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn deserialize_shakespeare() {
        let data = r#"
                         {
                           "success": {
                             "total": 1
                           },
                           "contents": {
                             "translated": "Thee did giveth mr. Tim a hearty meal,  but unfortunately what he did doth englut did maketh him kicketh the bucket.",
                             "text": "You gave Mr. Tim a hearty meal, but unfortunately what he ate made him die.",
                             "translation": "shakespeare"
                           }
                         }"#;
        let trans: Translation = serde_json::from_str(data).unwrap();
        assert_eq!(trans.success.total, 1, "Translation is successful");
        assert_eq!(trans.contents.translated, "Thee did giveth mr. Tim a hearty meal,  but unfortunately what he did doth englut did maketh him kicketh the bucket.", "Translated text is deserialized");
        assert_eq!(trans.contents.text, "You gave Mr. Tim a hearty meal, but unfortunately what he ate made him die.", "Original text is deserialized");
        assert_eq!(trans.contents.translation, "shakespeare", "Translation style is deserialized");
    }
}
