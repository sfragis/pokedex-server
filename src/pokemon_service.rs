use crate::dto::Pokemon;
use crate::funtranslations::client::{TranslationAuthor, TranslationClient};
use crate::pokeapi::client::{PokeClient, PokeError, PokemonId};

pub struct PokemonService {
    poke_client: PokeClient,
    trans_client: TranslationClient,
}

#[derive(thiserror::Error, Debug)]
pub enum PokemonError {
    #[error("Pokemon not found")]
    PokemonNotFound(String),
    #[error("Unexpected error occurred")]
    Unexpected,
}

impl warp::reject::Reject for PokemonError {}

impl PokemonService {
    /// Instances a new `PokemonService`
    pub fn new() -> Self {
        PokemonService {
            poke_client: PokeClient::new(),
            trans_client: TranslationClient::new(),
        }
    }

    /// Catches a Pokemon identified by its name
    ///
    /// # Arguments
    /// * `name` - Pokemon's name
    pub async fn catch(&self, name: &str) -> Result<Pokemon, PokemonError> {
        log::debug!("Catching Pokemon `{}` ...", name);
        let id = PokemonId::Name(name.to_string());
        self.poke_client.catch(&id).await
            .map(|species| species.into())
            .map_err(|err| match err {
                PokeError::PokemonNotFound => PokemonError::PokemonNotFound(name.to_string()),
                _ => {
                    log::error!("Unable to retrieve Pokemon identified by {}: {:?}", name, err);
                    PokemonError::Unexpected
                }
            })
    }

    /// Catches and translates a Pokemon identified by its name
    ///
    /// # Arguments
    /// * `name` - Pokemon's name
    pub async fn catch_and_translate(&self, name: &str) -> Result<Pokemon, PokemonError> {
        let res = self.catch(name).await;
        match res {
            Ok(poke) => {
                log::debug!("Translating Pokemon `{}`...", name);
                self.translate(poke).await
            },
            err => err
        }
    }

    fn author_for(&self, poke: &Pokemon) -> TranslationAuthor {
        if poke.is_legendary || poke.habitat.as_ref().filter(|h| *h == "cave").is_some() {
            TranslationAuthor::Yoda
        } else {
            TranslationAuthor::Shakespeare
        }
    }

    async fn translate(&self, poke: Pokemon) -> Result<Pokemon, PokemonError> {
        if let Some(desc) = &poke.description {
            let author = self.author_for(&poke);
            let trans_desc = self.safe_translate(desc, author).await;
            Ok(Pokemon {
                name: poke.name,
                description: Some(trans_desc),
                habitat: poke.habitat,
                is_legendary: poke.is_legendary,
            })
        } else {
            Ok(poke)
        }
    }

    async fn safe_translate(&self, text: &str, author: TranslationAuthor) -> String {
        self.trans_client.translate(text, author).await
            .map(|translation| translation.contents.translated)
            .unwrap_or_else(|err| {
                log::error!("Cannot translate Pokemon description: {:?}", err);
                text.to_string()
            })
    }
}
