use std::fs::{metadata, File};
use std::io;
use std::io::BufReader;
use serde::{Serialize, Deserialize};
use std::net::{ToSocketAddrs, SocketAddr};
use std::fmt;

/// Tests whether file identified by `path` exists.
/// # Arguments
/// * `path` - The file path
/// # Returns
/// `Ok` of void or an `Err` with the error string.
pub fn file_exists(path: &str) -> Result<(), &'static str> {
    if metadata(path).is_ok() {
        Ok(())
    } else {
        Err("File doesn't exist")
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SiteConfig {
    pub dir: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BindConfig {
    pub iface: Option<String>,
    pub port: Option<u16>,
}

impl fmt::Display for BindConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let blank = &"".to_string();
        let iface = self.iface.as_ref().unwrap_or(blank);
        let port = self.port.map(|p|p.to_string()).unwrap_or("".to_string());
        write!(f, "{}:{}", iface, port)
    }
}

impl Default for BindConfig {
    fn default() -> Self {
        BindConfig::new()
    }
}

impl BindConfig {
    pub fn new() -> BindConfig {
        BindConfig {
            port: Some(BIND_PORT),
            iface: Some(BIND_IFACE.to_string()),
        }
    }
}

impl ToSocketAddrs for BindConfig {
    type Iter = std::vec::IntoIter<SocketAddr>;

    fn to_socket_addrs(&self) -> io::Result<Self::Iter> {
        let default_bind_iface= BIND_IFACE.to_string();
        let i = self.iface.as_ref().unwrap_or(&default_bind_iface);
        let p = self.port.unwrap_or(BIND_PORT);
        (i.clone(), p).to_socket_addrs()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub bind: Option<BindConfig>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            bind: Some(def_bind())
        }
    }
}

/// A configuration error is due to either an I/O error or
/// a YAML parsing error.
#[derive(Debug)]
pub enum ConfigError {
    IoError(io::Error),
    YamlError(serde_yaml::Error),
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigError::IoError(e) => write!(f, "Io error: {}", e),
            ConfigError::YamlError(e) => write!(f, "YAML parsing error: {}", e)
        }
    }
}

pub const BIND_IFACE: &str = "localhost";
pub const BIND_PORT: u16 = 5000;

fn def_bind() -> BindConfig {
    BindConfig {
        iface: Option::from(BIND_IFACE.to_string()),
        port: Option::from(BIND_PORT),
    }
}

fn set_bind_defaults(cfg: BindConfig) -> BindConfig {
    BindConfig {
        port: cfg.port.or(Some(BIND_PORT)),
        iface: cfg.iface.or(Some(BIND_IFACE.to_string())),
    }
}

fn set_defaults(cfg: Config) -> Config {
    Config {
        bind: cfg.bind.map(set_bind_defaults).or_else(|| Some(def_bind()))
    }
}

/// Reads the configuration from the specified `conf_file` file.
/// # Arguments
/// * `conf_file` - A YAML configuration file
/// # Returns
/// The [Config] configuration structure or a [ConfigError] if something
/// goes wrong.
pub fn read_config(conf_file: &str) -> Result<Config, ConfigError> {
    let file = File::open(conf_file)
        .map_err(|io_err| ConfigError::IoError(io_err))?;
    serde_yaml::from_reader(BufReader::new(file))
        .map_err(|yml_err| ConfigError::YamlError(yml_err))
        .map(set_defaults)
}
