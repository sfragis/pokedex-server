use serde::{Deserialize, Serialize};
use crate::pokeapi::dto::PokemonSpecies;

/// A Pokemon description
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Pokemon {
    /// Pokemon name
    pub name: String,
    /// Pokemon description, optional
    pub description: Option<String>,
    /// Pokemon habitat, optional
    pub habitat: Option<String>,
    /// Whether this Pokemon is legendary or not
    pub is_legendary: bool,
}


impl From<PokemonSpecies> for Pokemon {
    fn from(species: PokemonSpecies) -> Self {
        Pokemon {
            name: species.name,
            description: species.flavor_text_entries.iter()
                .find(|t| t.language.name == "en")
                .map(|t| t.flavor_text.clone()),
            habitat: species.habitat.map(|h| h.name.clone()),
            is_legendary: species.is_legendary,
        }
    }
}

/// API error response
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiError {
    pub message: String
}
