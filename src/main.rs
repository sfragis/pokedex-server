use std::convert::Infallible;
use std::net::ToSocketAddrs;

use clap::clap_app;
use warp::{Filter, Rejection};
use warp::http::StatusCode;

use config::{file_exists, read_config};
use config::BindConfig;

use crate::config::Config;
use crate::dto::ApiError;
use crate::pokemon_service::{PokemonError, PokemonService};

mod config;
mod pokemon_service;
mod pokeapi;
mod funtranslations;
mod dto;

#[tokio::main]
async fn main() {
    let matches = clap_app!(pokedex =>
                            (version: "0.1.0")
                            (author: "Fabio G. Strozzi <fabio.strozzi@gmail.com>")
                            (about: "Pokedex Server")
                            (@arg config: -c --config +takes_value {file_exists} "Specifies a configuration file")
                            (@arg log_config: -l --("log-config") +takes_value {file_exists} "Specifies a logging configuration file")
                            (@subcommand run =>
                             (about: "Runs the server")
                            )
                            (@subcommand check =>
                             (about: "Checks configuration files and exit")
                            ))
        .try_get_matches()
        .unwrap_or_else(|e| {
            eprintln!("Invalid arguments");
            e.exit()
        });

    let config = if let Some(conf_file) = matches.value_of("config") {
        match read_config(conf_file) {
            Ok(c) => c,
            Err(e) => panic!("Cannot read configuration file `{}`: {}", conf_file, e)
        }
    } else {
        Config::default()
    };

    if let Some(log_conf_file) = matches.value_of("log_config") {
        println!("Log config file is: {}", log_conf_file);
        log4rs::init_file(log_conf_file, Default::default()).unwrap();
    }

    let bind_cfg = config.bind.unwrap_or(BindConfig::default());
    let maybe_socket_addr = bind_cfg
        .to_socket_addrs()
        .as_mut()
        .map(|iter| iter.next())
        .unwrap_or_else(|e| {
            panic!("Invalid binding interface: {:?}", e)
        });
    let socket_addr = match maybe_socket_addr {
        Some(sa) => sa,
        None => {
            panic!("No binding interface provided")
        }
    };

    let poke_api = warp::path("pokemon");

    let catch_and_translate = warp::path("translated")
        .and(warp::path::param::<String>())
        .and(warp::any().map(|| PokemonService::new()))
        .and_then(|name: String, srv: PokemonService| async move {
            log::debug!("New Pokemon catch and translate request: {}", name);
            srv.catch_and_translate(&name).await
                .map(|pokemon| warp::reply::json(&pokemon))
                .map_err(|e| warp::reject::custom(e))
        });

    let catch = warp::path::param::<String>()
        .and(warp::any().map(|| PokemonService::new()))
        .and_then(|name: String, srv: PokemonService| async move {
            log::debug!("New Pokemon catch request: {}", name);
            srv.catch(&name).await
                .map(|pokemon| warp::reply::json(&pokemon))
                .map_err(|e| warp::reject::custom(e))
        });

    let routes = warp::get()
        .and(
            poke_api.and(catch_and_translate)
                .or(poke_api.and(catch))
        ).recover(recover_on_error);

    log::info!("Running server ({})", bind_cfg);
    warp::serve(routes)
        .run(socket_addr)
        .await;
}


async fn recover_on_error(r: Rejection) -> Result<impl warp::Reply, Infallible> {
    let (status, message) = if let Some(e) = r.find::<PokemonError>() {
        match e {
            PokemonError::PokemonNotFound(name) => (StatusCode::NOT_FOUND, format!("Pokemon '{}' not found", name)),
            PokemonError::Unexpected => (StatusCode::INTERNAL_SERVER_ERROR, "Unexpected error occurred".to_string())
        }
    } else {
        (StatusCode::INTERNAL_SERVER_ERROR, "Unexpected error occurred".to_string())
    };

    let error = ApiError { message: message.to_string() };
    let json = warp::reply::json(&error);
    let json = warp::reply::with_header(json, "Content-type", "application/json");
    let rep = warp::reply::with_status(json, status);
    Ok(rep)
}
