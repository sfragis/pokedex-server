# PokeDex

A trivial Pokemon REST service that relies on https://funtranslations.com/ and https://pokeapi.co/docs/v2 to
retrieve data and translate Pokemon descriptions.

## How to install

A Rust environment is required, code has been developed and test for Rust 1.51.0.
Please head to https://www.rust-lang.org/tools/install to learn how to install Cargo and Rust.

Once Rust is installed, open a terminal and run these commands inside the cloned directory:

```shell
cargo build --release
```

## How to run

PokeDex can start without arguments, but two configuration files can be specified, one for the logging
and one for the general configuration.

Two sample (and optional) configuration files are provided with the source code:

- `pokedex.yml` - A YAML file that can be used to change the default port and binding interface: if no
  config file is specified, PokeDex binds to localhost and port 5000.
- `pokedex-log.yml` - A YAML file that be used to change the logging output: by default, logs are
  appended to the standard output and to a `logs/pokedex.jsonl` file (the latter uses a JSONL format,
  i.e., one JSON object per line). If no configuration file is specified, no log is written at all.

```shell
./target/release/pokedex -l pokedex-log.yml -c pokedex.yml
```

Now can be invoked using HTTPie or Curl:

```shell
http localhost:5000/pokemon/squirtle
# or
http localhost:5000/pokemon/translated/squirtle
```

## Using Docker

PokeDex can be built and run inside Docker using a multi-stage `Dockerfile`.
The build image and run image differ in order to avoid porting code and development tools to production.

To build PokeDex with Docker run this command:

```shell
docker build -t pokedex:0.1.0 .
```

To run PokeDex with Docker:

```shell
docker run --name pokedex -p 5000:5000 pokedex:0.1.0 
```

To stop it:

```shell
docker stop pokedex
```

## Things to improve for production

Many things need to be done to bring a piece of software like PokeDex to production:

- Collect logs using some cloud service like CloudWatch for AWS (or the equivalent one for Google) or use
  products like Kibana
- Collect some metrics: how many requests have successfully completed, how many have failed, how
  long did they take to complete, etc.
- Write some unit tests for `PokemonService` where both HTTP clients are mocked (I'd know how to do this
  in Java or Scala, but have no idea how to do it in Rust)
- Write integration tests and create a proper infrastructure where external HTTP service are simulated
- Cache requests, for instance using an LRU cache, to reduce the number of calls to external services
- Use a circuit-breaker for the fun translation service, so that when rate limit is reached no other calls
  are sent until the right amount of time has passed; this will reduce the pressure on the external system
  and limit the temporary bans.
- Make sure crates licenses are compatible with company guidelines
- Make sure only stable versions of the crates are used (PokeDex depends con Clap which is still beta)
- Perform some stress tests and identify bottlenecks
