FROM rust:1.51 as builder
WORKDIR /usr/src/pokedex
COPY . .
RUN cargo install --path .

FROM debian:bullseye-slim
RUN mkdir -p /srv/log /usr/local/etc/pokedex
COPY --from=builder /usr/local/cargo/bin/pokedex /usr/local/bin/pokedex
COPY pokedex-log.docker.yml /usr/local/etc/pokedex/log.yml
COPY pokedex.yml /usr/local/etc/pokedex/config.yml
RUN apt-get update && apt-get install -y openssl ca-certificates && rm -rf /var/lib/apt/lists/*
VOLUME /srv/log/
EXPOSE 5000
ENTRYPOINT ["/usr/local/bin/pokedex"]
CMD ["-l", "/usr/local/etc/pokedex/log.yml", "-c", "/usr/local/etc/pokedex/config.yml"]
